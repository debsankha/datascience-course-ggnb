{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "[Day 2 afternoon **_\"handling and plotting Big Data\"_**]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"top\"></a>Handling and plotting Big Data\n",
    "==="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"top\"></a>Outline\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* [Hierarchical Data Format HDF5](#HDF5)\n",
    "  * [Opening a File](#opening a file)\n",
    "  * [Creating a HDF5 file](#creating a file)\n",
    "  * [Attributes](#attributes)\n",
    "  * [Existence testing](#existance testing)\n",
    "  * [Iterating over a file](#iterating over a file)\n",
    "  * [A word on the library](#a word on the library)\n",
    "* [Exercise: HDF5](#exercise01)\n",
    "* [Interactive plotting with Bokeh](#bokeh)\n",
    "  * [Basic plotting interface](#bokeh intro)\n",
    "  * [Handling big data](#big data)\n",
    "  * [Interactive plotting](#interactive)\n",
    "* [Exercise: Bokeh](#exercise02)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To investigate data, we often need to visualize it in some way to make it understandable for humans. Often spending an hour or two just _looking_ at the various aspects of a dataset without quantifying much is a worthy investment - we call this _explorative analysis_. That way you get a feeling of what information your dataset holds and what questions would be worthwhile to ask.  \n",
    "\n",
    "After you have seen how Matplotlib and Seaborn can be used to visualize data, we want to show you how you can use python to handle and visualize truly big datasets using HDF5 and Bokeh."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Learning goals:** By the end or this lecture you will\n",
    "* know when to use a hierarchical format\n",
    "* know how to write and read data in hierarchical format\n",
    "* have an idea about how to deal with visualization of really big datasets\n",
    "* know how to create interactive plots with Bokeh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"HDF5\"></a>Hierarchical Data Format HDF5\n",
    "==="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hierarchical data formats have several advantages:\n",
    "* access to parts of large files without loading the whole file into RAM\n",
    "* intuitive and quick navigation inside the file following POSIX convenctions\n",
    "* storage of metadata right next to the data\n",
    "\n",
    "Why would we want that? Modern computers have 8-24 GB RAM. Everything that is larger needs to be _swapped_ from the hard drive where access times are really slow. Modern data-centered science is prone to create files much larger than that (particle collisions, cosmological observations, high resolution images...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### POSIX"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "People familiar with UNIX systems are intuitively familiar with the POSIX-style hierarchy \n",
    "\n",
    "```/path/to/file```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Different levels of the hierarchy are separated by forward-slashes. The single forward slash\n",
    "\n",
    "```/``` "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "is the \"root\" of the file, sub-levels can be accessed by using the full path beginning with root or relative paths."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"opening a file\"></a>Opening a (HDF5) File\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import h5py\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a hierarchical data file and access it in \"read and write\" mode\n",
    "f = h5py.File(\"testfile.hdf5\", \"a\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "01-data-wrangling.ipynb  04-handling-and-plotting-big-data.ipynb  testfile.hdf5\r\n",
      "02-pandas.ipynb\t\t exercises-02-pandas-solutions.ipynb\r\n",
      "03-seaborn.ipynb\t exercises-03-seaborn-solutions.ipynb\r\n"
     ]
    }
   ],
   "source": [
    "!ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some modes are\n",
    "* ```w```: write, overwrites file if it already exists, creates a new one otherwise\n",
    "* ```r```: read\n",
    "* ```r+```: read, write\n",
    "* ```a```: read, write, append (creates a new file if file doesn't exist, appends otherwise)\n",
    "\n",
    "Complete list at https://www.tutorialspoint.com/python/python_files_io.htm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/\n"
     ]
    }
   ],
   "source": [
    "print(f.name) # f is the \"root\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we open a file (any file!), we create a _handle_ of the file in the form of a Python object. This is different than accessing for example a numpy array that only exist in the part of the RAM that our Python script uses.\n",
    "\n",
    "The file exists on the hard drive and can be _created_, _read_ and _written_ to. For reading and writing information is piped to our script via a buffer. This is all handled in the background. The only thing we have to keep in mind is the kind of access we have to a file whose handle we use in our script."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"creating a file\"></a>Creating a HDF5 File\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "HDF5 file objects offer several methods to add, delete and modify their content. Their general structure can be broken down into\n",
    "* **datasets** which are basically tables (imagine NumPy arrays) and\n",
    "* **groups** which can be imagined like _folders_ that contain other groups or datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "fruits = f.create_group('fruits')\n",
    "vegetables = f.create_group('vegetables');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/fruits\n"
     ]
    }
   ],
   "source": [
    "print(fruits.name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/meat/cow\n"
     ]
    }
   ],
   "source": [
    "cow = f.create_group('meat/cow')\n",
    "print(cow.name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a dataset in a group\n",
    "oranges = fruits.create_dataset(\"oranges\", (100,10), dtype=\"i\")\n",
    "apples = fruits.create_dataset(\"apples\", (100,10), dtype=\"i\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/fruits/apples\n"
     ]
    }
   ],
   "source": [
    "print(apples.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important**:\n",
    "* groups work like python dictionaries\n",
    "* datasets work like NumPy arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<HDF5 dataset \"oranges\": shape (100, 10), type \"<i4\">"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# access in a key-value style\n",
    "fruits['oranges']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# access via field indices\n",
    "oranges[0,0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"attributes\"></a>Attributes\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the best features of HDF5 is that you can store metadata right next to the data it describes. All groups and datasets support attached named bits of data called _attributes_.\n",
    "\n",
    "Don't underestimate the power of this feature! It is very handy to have this information \n",
    "* in the same place and inseparable from the data (so it can't get lost)\n",
    "* easily accessible and iterable (so you don't require any awkward parsing from headers of text- or csv files)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "orange\n"
     ]
    }
   ],
   "source": [
    "# dictionary structure allows to save metadata right next to\n",
    "# the data as attributes\n",
    "oranges.attrs['color'] = 'orange'\n",
    "print(oranges.attrs['color'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Attributes can be attached to both ```group``` and ```dataset``` objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2018-02-18\n",
      "19\n"
     ]
    }
   ],
   "source": [
    "# note the 'attrs' proxy to get to the attributes\n",
    "f.attrs['date'] = '2018-02-18'\n",
    "f.attrs['temp'] = 19\n",
    "print(f.attrs['date'])\n",
    "print(f.attrs['temp'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"existence testing\"></a>Existance testing\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the keyword ```in``` we can check if a dataset or group exists within a file or group:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'oranges' in fruits"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'apples' in fruits"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# this will yield \"False\" because membership is only tested on\n",
    "# the accessed level (in this case root)\n",
    "'meat' in f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# this will yield \"True\" because by using the POSIX nomenclature\n",
    "# we access the subgroup level \"fruits\"\n",
    "'fruits/apples' in f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'pineapples' in fruits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"iterating over a file\"></a>Iterating over a file\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Groups also allow for iteration over their members"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Iterating over a level"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "fruits\n",
      "meat\n",
      "vegetables\n"
     ]
    }
   ],
   "source": [
    "for name in f:\n",
    "    print(name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "apples\n",
      "oranges\n"
     ]
    }
   ],
   "source": [
    "for name in fruits:\n",
    "    print(name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "fruits\n",
      "fruits/apples\n",
      "fruits/oranges\n",
      "meat\n",
      "meat/cow\n",
      "vegetables\n"
     ]
    }
   ],
   "source": [
    "f.visit(print)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Iterating over the whole file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "def MyPersonalPrintFunction(name):\n",
    "    print(\"jana's \" + name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = []\n",
    "def MyPersonalPrintFunction(name):\n",
    "    a.append(\"jana's \" + name)\n",
    "f.visit(MyPersonalPrintFunction)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[\"jana's fruits\", \"jana's fruits/apples\", \"jana's fruits/oranges\", \"jana's meat\", \"jana's meat/cow\", \"jana's vegetables\"]\n"
     ]
    }
   ],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"a word on the library\"></a>A word on the library\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```h5py``` very closely wraps the underlying ```C``` API. There is another rather similar library ```pytables```, which does the same thing but adds some higher level functionality (have a look at http://www.pytables.org/ for more details).\n",
    "\n",
    "The choice to introduce ```h5py``` and not ```pytables``` is pretty arbitrary. In the future they might be refactored into a different structure where ```h5py``` wraps the ```C``` API and ```pytables``` only accesses ```h5py```'s functionality. Watch out for changes if you decide to use one of these libraries!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"exercise01\"></a>Exercise: HDF5\n",
    "==="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. **Creating a file**\n",
    "  1. Create a file containing at least three groups\n",
    "  2. Fill the groups with at least three subgroups\n",
    "  3. Fill the subgroups with at least three datasets. Each dataset should contain a NxN array. N should be a random number between 50 and 100. Fill each array with random numbers drawn from a gaussian distribution with different mean and standard deviation respectively.\n",
    "  4. Attach an attribute for the mean and standard deviation to each dataset.\n",
    "2. **Iterating over a file**\n",
    "  1. Iterate over all datasets in the file and calculate the number of entries in each array, the mean and standard deviation.\n",
    "  2. Compare the mean and standard deviation you calculated to the attributes you assigned to the dataset and print both to the command line.\n",
    "3. **(Optional)**\n",
    "    1. investigate about graphical user interfaces to inspect HDF5 files (for example ViTables).\n",
    "    2. See if you can get it to work within a Jupyter notebook (hint: you can use ```pip install``` in the terminal to install new packages to your jupyter-hub server if needed.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"bokeh\"></a>Interactive plotting with Bokeh\n",
    "==="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"bokeh intro\"></a>Basic plotting interface\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# output_notebook() tells bokeh to embed the plots. Other options are\n",
    "# output_file() and output_server()\n",
    "import pandas as pd\n",
    "from bokeh.io import output_notebook, show\n",
    "from bokeh.plotting import figure\n",
    "output_notebook()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bokeh's approach to plotting is rather similar than Matplotlib's. The only difference is, that it uses _symbol_ based plotting functions rather than _plot type_ based plotting functions. For example\n",
    "* where matplotlib would use **scatterplot()**, bokeh uses **circles()**\n",
    "* where matplotlib uses **plot()**, boke uses **lines()**. \n",
    "\n",
    "Similar to matplotlib bokeh creates a canvas on which the plotted objects reside using the **figure()** function. It also returns the plotted objects when calling the respective plotting functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create the canvas\n",
    "fig = figure(plot_width=400, plot_height=400)\n",
    "\n",
    "# add a circle renderer with a size, color, and alpha\n",
    "fig.circle([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], \\\n",
    "    size=15, line_color=\"navy\", fill_color=\"orange\", fill_alpha=0.5)\n",
    "\n",
    "show(fig) # show the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"big data\"></a>Handling big data\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are limitations to how much data can be shown in the way we used to in the examples above. Most web browsers can handle up to about 100,000 or 200,000 datapoints in a Bokeh plot before they will slow down or have memory issues. If we have larger datasets than that, one option is to use the ```datashader``` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datashader as ds\n",
    "import datashader.transfer_functions as tf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### When _not_ to use datashader\n",
    "* when plotting less than $10^6$ datapoints\n",
    "* when _every_ datapoint matters (and not just distributions)\n",
    "* when full interactivity with _every_ datapoint is necessary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### When _to_ to use datashader\n",
    "* when Matplotlib/Seaborn/Bokeh run into memory issues\n",
    "* when _distribution_ matters more than individual points\n",
    "* when sampling/binning to understand the distribution anyways"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### How does it work?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![datashader](https://raw.githubusercontent.com/bokeh/datashader/master/docs/images/pipeline2.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* datashader renders data into screen-sized aggregate arrays\n",
    "* from the aggregates an image can be constructed and then embedded into a bokeh plot\n",
    "* this way only the fixed-size image needs to be sent to the browser  \n",
    "\n",
    "The datashader currently supports\n",
    "* scatterplots\n",
    "* heatmaps\n",
    "* timeseries\n",
    "* trajectories\n",
    "* rasters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create five gaussian distributions with 10⁶ points each and sum them \n",
    "# to make up our dataset\n",
    "seed=5\n",
    "np.random.seed(seed)\n",
    "num=1000000\n",
    "\n",
    "# the distributions are centered at (2,2), (2,-2), (-2,-2), (-2,2) and\n",
    "# (0,0) and have a sigma of 0.01, 0.1, 0.5, 0.75 and 1.0 respectively\n",
    "dists = {cat: pd.DataFrame(dict(x=np.random.normal(x,s,num),\n",
    "                                y=np.random.normal(y,s,num),\n",
    "                                val=val,cat=cat))\n",
    "         for x,y,s,val,cat in \n",
    "         [(2,2,0.01,10,\"d1\"), \\\n",
    "          (2,-2,0.1,20,\"d2\"), \\\n",
    "          (-2,-2,0.5,30,\"d3\"), \\\n",
    "          (-2,2,0.75,40,\"d4\"), \\\n",
    "          (0,0,1.0,50,\"d5\")]}\n",
    "\n",
    "# dislay the dataframe containing the data\n",
    "df = pd.concat(dists,ignore_index=True)\n",
    "df[\"cat\"]=df[\"cat\"].astype(\"category\")\n",
    "df.tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the first step, we define\n",
    "* the size of the window we want to project our data into\n",
    "* the reduction function we want to use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create the canvas with a height/width in screen units to which the\n",
    "# height/width in data units is projected\n",
    "canvas = ds.Canvas(plot_width=300, plot_height=300, \\\n",
    "                   x_range=(-4,4), y_range=(-4,4))\n",
    "\n",
    "# create an aggregate of the x and y columns of the data using the \n",
    "# ds.count() function to aggregate\n",
    "agg = canvas.points(df, 'x', 'y', agg=ds.count())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_aggregation function examples:_  \n",
    "* **count():** integer count of datapoints for each pixel  \n",
    "* **any():** 1 for each pixel if it containy any datapoint, 0 otherwise\n",
    "* **count_cat(column):** count datapoints per category"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the aggregate created we can use the datashader to actually display it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tf.shade(agg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The aggregate itself is a numpy array that we can filter and manipulate to our liking:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "agg_reduced = agg.where(agg >= np.percentile(agg,50))\n",
    "tf.shade(agg_reduced)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the categorical aggregate or a colormap we can make more sense of the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "color_key = dict(d1='blue', d2='green', d3='red', d4='orange', d5='purple')\n",
    "agg_category = canvas.points(df, 'x', 'y', ds.count_cat('cat'))\n",
    "tf.shade(agg_category, color_key)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tf.shade(agg,cmap=[\"darkred\", \"blue\"],how='log')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"interactive\"></a>Interactive plots\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Zooming and panning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bokeh provides specific support for datashader to allow for fully interactive zooming and panning to explore the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import bokeh.plotting as bp\n",
    "from datashader.bokeh_ext import InteractiveImage\n",
    "bp.output_notebook()\n",
    "\n",
    "# create a new and larger canvas with interactive tools enabled\n",
    "p = bp.figure(tools='pan,wheel_zoom,reset', x_range=(-5,5),\\\n",
    "              y_range=(-5,5))\n",
    "\n",
    "# create a callback function: dynspread spreads pixels in an \n",
    "# image dynamically based on the image density.\n",
    "def image_callback(x_range, y_range, w, h):\n",
    "    cvs = ds.Canvas(plot_width=w, plot_height=h, x_range=x_range, \\\n",
    "                    y_range=y_range)\n",
    "    agg = cvs.points(df, 'x', 'y', ds.count_cat('cat'))\n",
    "    img = tf.shade(agg, color_key)\n",
    "    return tf.dynspread(img, threshold=0.25)\n",
    "\n",
    "# add the callback function to the interactive image\n",
    "InteractiveImage(p, image_callback)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bokeh supports direct integration with a small basic widget set. Thse can be used in conjunction with a Bokeh Server, or with CustomJS models to add more interactive capability to the plots.  \n",
    "\n",
    "Widgets that have values associated can have small JavaScript actions attached to them. These actions (also referred to as \"callbacks\") are executed whenever the widget's value is changed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.layouts import column\n",
    "from bokeh.models import CustomJS, ColumnDataSource, Slider\n",
    "\n",
    "x = [x*0.005 for x in range(0, 200)]\n",
    "y = x\n",
    "\n",
    "source = ColumnDataSource(data=dict(x=x, y=y))\n",
    "\n",
    "plot = figure(plot_width=400, plot_height=400)\n",
    "plot.line('x', 'y', source=source, line_width=3, line_alpha=0.6)\n",
    "\n",
    "callback = CustomJS(args=dict(source=source), code=\"\"\"\n",
    "    var data = source.get('data');\n",
    "    var f = cb_obj.get('value')\n",
    "    x = data['x']\n",
    "    y = data['y']\n",
    "    for (i = 0; i < x.length; i++) {\n",
    "        y[i] = Math.pow(x[i], f)\n",
    "    }\n",
    "    source.trigger('change');\n",
    "\"\"\")\n",
    "\n",
    "slider = Slider(start=0.1, end=4, value=1, step=.1, \\\n",
    "                title=\"power\", callback=callback)\n",
    "\n",
    "layout = column(slider, plot)\n",
    "\n",
    "show(layout)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a name=\"exercise02\"></a>Exercise: Bokeh\n",
    "==="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. **Big data**\n",
    "  1. Explore the 'big' dataset we created in the example further (or create a new one): check at what point matplotlib runs into serious efficiency trouble (HINT: %timeit magic)\n",
    "  2. Try datashader's capabilities for a timeseries - what aggregation functions make sense?\n",
    "  3. Try datashader's capabilities for trajectories (connected points)\n",
    "  \n",
    "  \n",
    "2. **Interactive plot**\n",
    "  1. Have a look at the Bokeh gallery http://bokeh.pydata.org/en/latest/docs/gallery.html#gallery and get some ideas about Bokehs capabilities.\n",
    "  2. Make an interactive plot that realizes a toggle\n",
    "  3. Make an interactive plot that realizes a dropdown\n",
    "  \n",
    "  \n",
    "3. **(Optional) Container at Sea**\n",
    "    1. Download the datset at [http://data.deutschebahn.com/dataset/data-sensordaten-schenker-seefrachtcontainer](http://data.deutschebahn.com/dataset/data-sensordaten-schenker-seefrachtcontainer). The dataset contains GPS traces from contains in Cargo ships. \n",
    "    2. Visualize the trajectories on top of a world map by using `bokeh` or `plotly`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[top](#top)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Credits & References"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[**h5py**: kudos to **the h5py quick start guide** at http://docs.h5py.org/en/latest/quick.html#install]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[**bokeh**: kudos to **the bokeh tutorials** at http://nbviewer.jupyter.org/github/bokeh/bokeh-notebooks/tree/master/tutorial/]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
